﻿using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public partial class ToolDomain
    {
        private IToolRepository _toolRepo;
        private DbContext _context;

        public ToolDomain(IToolRepository toolRepository, DbContext context)
        {
            _toolRepo = toolRepository;
            _context = context;
        }

        public List<ToolGeneralModel> GetAll()
        {
            List<ToolGeneralModel> lists = new List<ToolGeneralModel>();

            lists = _toolRepo.Get().Select(s => new ToolGeneralModel
            {
                id = s.Id,
                Name = s.Name,
                Description = s.Description,
                Image = s.Image,
                Quantity = s.Quantity,
                Status = s.Status
            }).ToList();
            return lists;
        }

        public string CreateTool(ToolCreateModel model)
        {
            var newTool = _toolRepo.CreateTool(model,model.Image);
            _context.SaveChanges();
            return newTool.Id;
        }


        public bool EditTool(string toolId, ToolUpdateModel model)
        {
            bool check = false;
            var currentTool = _toolRepo.Get().FirstOrDefault(s => s.Id == toolId);
            if(currentTool != null)
            {
                _toolRepo.UpdateTool(currentTool, model, model.Image);
                _context.SaveChanges();
                check = true;
            }
            return check;
        }

        public bool Delete(string toolId)
        {
            bool check = false;
            var currentTool = _toolRepo.Get().FirstOrDefault(s => s.Id == toolId);
            if (currentTool != null)
            {
                currentTool.Status = ToolStatus.DELETED;
                _toolRepo.Update(currentTool);
                _context.SaveChanges();
                check = true;
            }
            return check;
        }

        public bool BorrowTool(string toolId, int quantity)
        {
            var check = false;

            var currentTool = _toolRepo.Get().FirstOrDefault(s => s.Id == toolId);
            if(currentTool != null)
            {
                if (currentTool.Quantity >= quantity)
                {
                    currentTool.Quantity = currentTool.Quantity - quantity;
                    if (currentTool.Quantity == 0)
                    {
                        currentTool.Status = ToolStatus.OUT_OF_STOCK;
                    }
                    else
                    {
                        currentTool.Status = ToolStatus.AVAILABLE;
                    }
                    _toolRepo.Update(currentTool);
                    _context.SaveChanges();
                    check = true;
                }
                if (currentTool.Quantity == 0)
                {
                    throw new Exception("Out Of Stock");
                }
                if(currentTool.Quantity < quantity)
                {
                    throw new Exception("Not Enough");
                }
            }
            return check;
        }
    }
}
