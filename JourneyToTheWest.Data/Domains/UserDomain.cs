﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace JourneyToTheWest.Data.Extensions
{
    public class UserDomain
    {
        private IUserRepository _userRepo;
        private IUserRoleRepository _userRoleRepo;
        private IRoleRepository _roleRepo;
        private DbContext _context;

        public UserDomain(IUserRepository userRepository, IUserRoleRepository userRoleRepository, IRoleRepository roleRepository, DbContext context)
        {
            _userRepo = userRepository;
            _userRoleRepo = userRoleRepository;
            _roleRepo = roleRepository;
            _context = context;
        }

        private bool AddRoleAdmin(string userId)
        {
            bool check = false;
            var user = _userRepo.FindById(userId);

            if(user != null)
            {
                _userRoleRepo.AddRole(user.Id, RoleIdDetails.ROLE_ADMIN_ID);
                check = true;
                _context.SaveChanges();
            }

            return check;
        }

        private bool AddRoleActor(string userId)
        {
            bool check = false;
            var user = _userRepo.FindById(userId);

            if (user != null)
            {
                _userRoleRepo.AddRole(user.Id, RoleIdDetails.ROLE_ACTOR_ID);
                check = true;
                _context.SaveChanges();
            }

            return check;
        }

        public bool CreateUser(UserCreateModel model)
        {
            var user = _userRepo.CreateUser(model);
            _context.SaveChanges();
            return AddRoleActor(user.Id);
        }

        public bool CreateAdmin(UserCreateModel model)
        {
            var user = _userRepo.CreateUser(model);
            _context.SaveChanges();
            return AddRoleAdmin(user.Id);
        }

        public UserViewModel Login(UserLoginModel model)
        {
            var user = _userRepo.Get().FirstOrDefault(u => u.Username == model.Username && u.Password == model.Password);
            if(user != null)
            {
                var result = new UserViewModel
                {
                    Id = user.Id,
                    Username = user.Username
                };
                return result;
            }
            return null;
        }

        private Roles GetRoleOfUser(string userId)
        {
            var role = _userRoleRepo.Get().FirstOrDefault(r => r.UserId == userId);
            if(role != null)
            {
                return role.Role;
            }
            return null;
        }

        public string GenerateToken(UserViewModel model)
        {
            var handler = new JwtSecurityTokenHandler();
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, model.Username));
            claims.Add(new Claim("Id", model.Id));
            var role = GetRoleOfUser(model.Id);
            if(role != null)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);
            var key = Encoding.ASCII.GetBytes("PRM_Fucking_Secret");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claimsIdentity,
                Issuer = "PRM_Issuer",
                Audience = "PRM_Audience",
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var newToken = handler.CreateToken(tokenDescriptor);
            return handler.WriteToken(newToken);
        }

        public CurrentUserModel GetCurrentUserDetail(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var result = handler.ReadJwtToken(token) as JwtSecurityToken;
            var currentClaims = result.Claims.ToList();
            string id = currentClaims.FirstOrDefault(t => t.Type == "Id").Value;
            string role = currentClaims.FirstOrDefault(t => t.Type == "role").Value;
            var user = _userRepo.Get().FirstOrDefault(s => s.Id == id);
            if(user != null)
            {
                if (user.Actor != null)
                {
                    var model = new CurrentUserModel
                    {
                        Id = user.Id,
                        Fullname = role == "Admin" ? null : user.Actor.Name,
                        Role = role,
                        Avatar = role == "Admin" ? null : user.Actor.Avatar,
                        Username = user.Username,
                        Status = role == "Admin" ? true : user.Status.GetValueOrDefault(),
                        Updated = user.UpdatedProfile.GetValueOrDefault()
                    };
                    return model;
                }
                else
                {
                    var model = new CurrentUserModel
                    {
                        Id = user.Id,
                        Role = role,
                        Username = user.Username,
                        Updated = user.UpdatedProfile.GetValueOrDefault(),
                        Status = user.Status.GetValueOrDefault()
                    };
                    return model;
                }
                
            }

            return null;
        }
    }
}
