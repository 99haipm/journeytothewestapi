﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public class ToolFilmDomain
    {
        private IToolFilmRepository _toolFilmRepo;
        private DbContext _context;
        private ToolDomain _toolDomain;
        public ToolFilmDomain(IToolFilmRepository toolFilmRepository, DbContext context,ToolDomain toolDomain)
        {
            _toolFilmRepo = toolFilmRepository;
            _context = context;
            _toolDomain = toolDomain;
        }


        public bool CreateToolFilms(ToolFilmViewModel model, string filmId)
        {
            var check = false;
            using (var trans = _context.Database.BeginTransaction())
            {
                foreach(BorrowToolModel currentModel in model.Tools)
                {
                    _toolFilmRepo.CreateToolFilm(currentModel, filmId);
                    _toolDomain.BorrowTool(currentModel.ToolId, currentModel.Quantity);
                    _context.SaveChanges();
                }
                trans.Commit();
                check = true;
            }
            return check;
        }

        public List<ToolOfFilmModel> GetToolsOfFilm(string filmId)
        {
            List<ToolOfFilmModel> result = new List<ToolOfFilmModel>();

            var toolFilm = _toolFilmRepo.Get().Where(s => s.FilmId == filmId);
            foreach(var item in toolFilm)
            {
                ToolOfFilmModel model = new ToolOfFilmModel
                {
                    Id = item.Id,
                    FilmId = filmId,
                    FilmName = item.Film.Name,
                    ToolId = item.ToolId,
                    ToolName = item.Tool.Name,
                    ToolImage = item.Tool.Image,
                    BorrowFrom = DateTime.FromFileTime(item.BorrowFrom.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                    BorrowTo = DateTime.FromFileTime(item.BorrowTo.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                    Quantity = item.Quantity.GetValueOrDefault()
                };
                result.Add(model);
            }
            return result;
        }

        public bool DeleteToolOfFilm(string toolFilmId)
        {
            bool check = false;
            var currentToolFilm = _toolFilmRepo.Get().FirstOrDefault(s => s.Id == toolFilmId);
            if(currentToolFilm != null)
            {
                _toolFilmRepo.Remove(currentToolFilm);
                _toolDomain.BorrowTool(currentToolFilm.ToolId, currentToolFilm.Quantity.GetValueOrDefault()*(-1));
                _toolFilmRepo.SaveChanges();
                check = true;
            }
            return check;
        }
    }
}
