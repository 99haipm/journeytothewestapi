﻿using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public class FilmDomain
    {
        private IFilmRepository _filmRepo;
        private DbContext _context;
        private ICharactorRepository _charactorRepo;
        public FilmDomain(IFilmRepository filmRepo,DbContext context, ICharactorRepository charactorRepository)
        {
            _context = context;
            _filmRepo = filmRepo;
            _charactorRepo = charactorRepository;
        }


        public string CreateFilm(FilmCreateModel model)
        {
            var newFilm = _filmRepo.CreateFilm(model);
           
            _context.SaveChanges();
            return newFilm.Id;
        }


        public bool DeleteCharacter(string characterId)
        {
            bool check = false;

            var currentCharacter = _charactorRepo.Get().FirstOrDefault(s => s.Id == characterId);
            if(currentCharacter != null)
            {
                _charactorRepo.Remove(currentCharacter);
                _charactorRepo.SaveChanges();
                check = true;
            }
            return check;
        }


        public List<FilmGeneralModel> GetAll()
        {
            List<FilmGeneralModel> lists = new List<FilmGeneralModel>();
            lists = _filmRepo.Get().Select(s => new FilmGeneralModel
            {
                Id = s.Id,
                Name = s.Name,
                Description = s.Description,
                BeginTime = DateTime.FromFileTime(s.BeginTime.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                EndTime = DateTime.FromFileTime(s.EndTime.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                //BeginTime = s.BeginTime.GetValueOrDefault(),
                //EndTime = s.EndTime.GetValueOrDefault(),
                Location = s.Location,
                Status = s.Status
            }).ToList();
            return lists;
        }

        public List<FilmGeneralModel> GetFinishFilmOfActor(string actorId)
        {
            List<FilmGeneralModel> lists = new List<FilmGeneralModel>();
            lists = _charactorRepo.Get().Where(s => s.Film.Status == FilmStatus.FINISHED && s.ActorId == actorId).Select(s => new FilmGeneralModel
            {
                Id = s.Film.Id,
                Name = s.Film.Name,
                Description = s.Film.Description,
                BeginTime = DateTime.FromFileTime(s.Film.BeginTime.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                EndTime = DateTime.FromFileTime(s.Film.EndTime.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                //BeginTime = s.BeginTime.GetValueOrDefault(),
                //EndTime = s.EndTime.GetValueOrDefault(),
                Location = s.Film.Location,
                Status = s.Film.Status
            }).ToList();
            return lists;
        }

        public bool FinishFilm(string filmId)
        {
            var check = false;
            var currentFilm = _filmRepo.Get().FirstOrDefault(s => s.Id == filmId);
            if(currentFilm != null)
            {
                currentFilm.Status = FilmStatus.FINISHED;
                currentFilm.EndTime = DateTime.Now.ToFileTime();
                _filmRepo.Update(currentFilm);
                _context.SaveChanges();
                check = true;
            }

            return check;
        }

        public bool UpdateFilm(FilmUpdateModel model, string id)
        {
            var check = false;
            var currentFilm = _filmRepo.Get().FirstOrDefault(s => s.Id == id);
            if(currentFilm != null)
            {
                var film = _filmRepo.UpdateFilm(currentFilm, model);
                _filmRepo.Update(film);
                _context.SaveChanges();
                check = true;
            }

            return check;
        }

        public bool DeleteFilm(string id)
        {
            var check = false;
            var currentFilm = _filmRepo.Get().FirstOrDefault(s => s.Id == id);
            if(currentFilm != null)
            {
                currentFilm.Status = FilmStatus.DELETED;
                _filmRepo.Update(currentFilm);
                _context.SaveChanges();
                check = true;
            }
            return check;
        }

        public bool AddActorToFilm(CharactorViewModel model)
        {
            var check = false;

            var currentCharactor = _charactorRepo.CreateCharacter(model, model.FilmScript);
            var currentFilm = _filmRepo.Get().FirstOrDefault(s => s.Id == model.FilmId);
            if(currentCharactor != null && currentFilm != null)
            {
                currentFilm.Status = FilmStatus.PROCESS;
                _filmRepo.Update(currentFilm);
                _context.SaveChanges();
                check = true;
            }
            return check;
        }

        public List<CharactorOfFilmModel> GetCharactorOfFilm(string filmId)
        {
            List<CharactorOfFilmModel> result = new List<CharactorOfFilmModel>();

            var currentList = _charactorRepo.Get().Where(s => s.FilmId == filmId);
            foreach(var item in currentList)
            {
                var elm = new CharactorOfFilmModel
                {
                    FilmId = item.FilmId,
                    FilmName = item.Film.Name,
                    ActorId = item.ActorId,
                    ActorName = item.Actor.Name,
                    CharactorName = item.Name,
                    Description = item.Description,
                    FilmScript = item.FilmScript,
                    ActorImage = item.Actor.Avatar,
                    CharacterId = item.Id,
                };
                result.Add(elm);
            }
            return result;
        }

        public string GetFilmScrip(string characterId)
        {
            var currentCharacter = _charactorRepo.Get().FirstOrDefault(s => s.Id == characterId);
            if(currentCharacter != null)
            {
                return currentCharacter.FilmScript;
            }
            return null;
        }

        public List<CharacterViewModel> GetCharactorOfActor(string actorId)
        {
            var currentCharacter = _charactorRepo.Get().Where(s => s.ActorId == actorId)
                .Select(s => new CharacterViewModel
                {
                    FilmId = s.FilmId,
                    FilmName = s.Film.Name,
                    CharacterName = s.Name,
                    FilmScript = s.FilmScript
                });

            if(currentCharacter != null)
            {
                return currentCharacter.ToList();
            }
            else
            {
                return null;
            }
        }

        public List<FilmGeneralModel> GetAllFilmOfActor(string id)
        {
            List<FilmGeneralModel> lists = new List<FilmGeneralModel>();
            lists = _charactorRepo.Get().Where(s => s.ActorId == id && s.Film.Status != FilmStatus.FINISHED && s.Film.Status != FilmStatus.DELETED)
                        .Select(s => new FilmGeneralModel
                        {
                            Id = s.FilmId,
                            Name = s.Film.Name,
                            Description = s.Film.Description,
                            BeginTime = DateTime.FromFileTime(s.Film.BeginTime.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                            EndTime = DateTime.FromFileTime(s.Film.EndTime.GetValueOrDefault()).ToString("MM/dd/yyyy h:mm tt"),
                            //BeginTime = s.BeginTime.GetValueOrDefault(),
                            //EndTime = s.EndTime.GetValueOrDefault(),
                            Location = s.Film.Location,
                            Status = s.Film.Status
                        }).ToList();


            return lists;
        }

    }
}
