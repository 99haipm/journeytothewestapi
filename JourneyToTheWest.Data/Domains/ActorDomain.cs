﻿using JourneyToTheWest.Data.Extensions;
using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public class ActorDomain
    {
        private IActorRepository _actorRepo;
        private IUserRepository _userRepo;
        private IHistoryRepository _historyRepo;
        public ActorDomain(IActorRepository actorRepository, IUserRepository userRepository, IHistoryRepository historyRepo)
        {
            _actorRepo = actorRepository;
            _userRepo = userRepository;
            _historyRepo = historyRepo;
        }

        public string Create(ActorCreateModel model)
        {
            string result;
            result = _actorRepo.CreateActor(model,model.Avatar).Id;
            var user = _userRepo.Get().FirstOrDefault(s => s.Id == result);
            if(user != null)
            {
                user.UpdatedProfile = true;
                _userRepo.Update(user);
            }
            _actorRepo.SaveChanges();
            return result;
        }

        public ActorViewModel GetById(string id)
        {
            var result = _actorRepo.GetById(id);
            if(result != null)
            {
                var viewModel = new ActorViewModel
                {
                    Id = result.Id,
                    Name = result.Name,
                    Email = result.Email,
                    Description = result.Description,
                    Phone = result.Phone,
                    Avatar = result.Avatar,
                    Status = result.Status
                };
                return viewModel;
            }
            return null;
        }

        public string EditActor(string id, ActorUpdateModel model)
        {
            var result = _actorRepo.EditActor(model, id);
            var history = _historyRepo.CreateHisotry(id, "Update");
            _actorRepo.SaveChanges();
            return result.Id;
        }


        public object GetActor(ActorFilter filter, string sort, int page, int limit)
        {
            var query = _actorRepo.Get();
            int totalPage = 0;
            if (limit > -1)
            {
                totalPage = query.Count() / limit;
            }
            return query.GetData(filter, sort, page, limit, totalPage);
        }

        public void DeleteActor(string id)
        {
            var actor = _actorRepo.GetById(id);
            var user = _userRepo.Get().FirstOrDefault(s => s.Id == id);
            if (actor != null && user != null)
            {
                actor.Status = false;
                user.Status = false;
                _userRepo.Update(user);
                _actorRepo.Update(actor);
                _actorRepo.SaveChanges();
            }
        }

        public void UnBanActor(string id)
        {
            var actor = _actorRepo.GetById(id);
            var user = _userRepo.Get().FirstOrDefault(s => s.Id == id);
            if (actor != null && user != null)
            {
                actor.Status = true;
                user.Status = true;
                _userRepo.Update(user);
                _actorRepo.Update(actor);
                _actorRepo.SaveChanges();
            }
        }

    }
}
