﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.Extensions;
using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.DI
{
    public static class G
    {
		public static void ConfigureIoC(IServiceCollection services)
		{
			//MapperConfigs.Add(cfg =>
			//{

			//});
			//ConfigureAutomapper();
			////IoC
			services.AddScoped<DbContext, JourneyToWestContext>()
					.AddScoped<IActorRepository, ActorRepository>()
					.AddScoped<IUserRepository, UserRepository>()
					.AddScoped<IRoleRepository, RoleRepository>()
					.AddScoped<IUserRoleRepository, UserRoleRepository>()
					.AddScoped<IFilmRepository, FilmRepository>()
					.AddScoped<IToolRepository, ToolRepository>()
					.AddScoped<IToolFilmRepository, ToolFilmRepository>()
					.AddScoped<ICharactorRepository, CharactorRepository>()
					.AddScoped<IHistoryRepository,HistoryRepository>()
					.AddScoped<ToolDomain>()
					.AddScoped<UserDomain>()
					.AddScoped<ActorDomain>()
					.AddScoped<FilmDomain>()
					.AddScoped<ToolFilmDomain>();
		}
	}
}
