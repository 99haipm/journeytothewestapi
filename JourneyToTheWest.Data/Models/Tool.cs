﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class Tool
    {
        public Tool()
        {
            ToolFilm = new HashSet<ToolFilm>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }

        public virtual ICollection<ToolFilm> ToolFilm { get; set; }
    }
}
