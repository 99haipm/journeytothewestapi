﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class Film
    {
        public Film()
        {
            Character = new HashSet<Character>();
            ToolFilm = new HashSet<ToolFilm>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public int? TimeOfFilm { get; set; }
        public long? BeginTime { get; set; }
        public long? EndTime { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Character> Character { get; set; }
        public virtual ICollection<ToolFilm> ToolFilm { get; set; }
    }
}
