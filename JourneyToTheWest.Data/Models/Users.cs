﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class Users
    {
        public Users()
        {
            HistoryModifyByNavigation = new HashSet<History>();
            HistoryUser = new HashSet<History>();
            UserRole = new HashSet<UserRole>();
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? Status { get; set; }
        public bool? UpdatedProfile { get; set; }

        public virtual Actor Actor { get; set; }
        public virtual ICollection<History> HistoryModifyByNavigation { get; set; }
        public virtual ICollection<History> HistoryUser { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
