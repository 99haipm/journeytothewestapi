﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class History
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string UserId { get; set; }

        public virtual Users ModifyByNavigation { get; set; }
        public virtual Users User { get; set; }
    }
}
