﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class Actor
    {
        public Actor()
        {
            Character = new HashSet<Character>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }

        public virtual Users IdNavigation { get; set; }
        public virtual ICollection<Character> Character { get; set; }
    }
}
