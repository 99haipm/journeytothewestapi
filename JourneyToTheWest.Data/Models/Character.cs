﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class Character
    {
        public string Id { get; set; }
        public string FilmId { get; set; }
        public string ActorId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string FilmScript { get; set; }

        public virtual Actor Actor { get; set; }
        public virtual Film Film { get; set; }
    }
}
