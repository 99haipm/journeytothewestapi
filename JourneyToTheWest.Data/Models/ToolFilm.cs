﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class ToolFilm
    {
        public string Id { get; set; }
        public string ToolId { get; set; }
        public string FilmId { get; set; }
        public long? BorrowFrom { get; set; }
        public long? BorrowTo { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }

        public virtual Film Film { get; set; }
        public virtual Tool Tool { get; set; }
    }
}
