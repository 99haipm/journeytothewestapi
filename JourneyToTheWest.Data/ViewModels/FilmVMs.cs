﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.ViewModels
{
    class FilmVMs
    {
    }

    public class FilmCreateModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("begin-time")]
        public DateTime BeginTime { get; set; }
        [JsonProperty("end-time")]
        public DateTime EndTime { get; set; }
    }

    public class FilmGeneralModel
    {

        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("begin-time")]
        public string BeginTime { get; set; }
        [JsonProperty("end-time")]
        public string EndTime { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }

    }

    public class FilmUpdateModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("begin-time")]
        public DateTime BeginTime { get; set; }
        [JsonProperty("end-time")]
        public DateTime EndTime { get; set; }
    }

    public class ToolFilmViewModel
    {
        [JsonProperty("film-id")]
        public string FilmId { get; set; }
        [JsonProperty("tools")]
        public BorrowToolModel[] Tools { get; set; }

    }

    public class BorrowToolModel
    {
        [JsonProperty("tool-id")]
        public string ToolId { get; set; }
        [JsonProperty("borrow-from")]
        public DateTime BorrowFrom { get; set; }
        [JsonProperty("borrow-to")]
        public DateTime BorrowTo { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }


    public class ToolOfFilmModel
    {
        [JsonProperty("tool-film-id")]
        public string Id { get; set; }
        [JsonProperty("tool-id")]
        public string ToolId { get; set; }
        [JsonProperty("tool-name")]
        public string ToolName { get; set; }
        [JsonProperty("tool-image")]
        public string ToolImage { get; set; }
        [JsonProperty("film-id")]
        public string FilmId { get; set; }
        [JsonProperty("film-name")]
        public string FilmName { get; set; }
        [JsonProperty("borrow-from")]
        public string BorrowFrom { get; set; }
        [JsonProperty("borrow-to")]
        public string BorrowTo { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }

    public class CharactorViewModel
    {
        [JsonProperty("film-id")]
        public string FilmId { get; set; }
        [JsonProperty("actor-id")]
        public string ActorId { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("film-script")]
        public string FilmScript { get; set; }
    }

    public class CharactorOfFilmModel
    {
        [JsonProperty("character-id")]
        public string CharacterId { get; set; }
        [JsonProperty("film-id")]
        public string FilmId { get; set; }
        [JsonProperty("actor-id")]
        public string ActorId { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("charactor-name")]
        public string CharactorName { get; set; }
        [JsonProperty("film-script")]
        public string FilmScript { get; set; }
        [JsonProperty("actor-name")]
        public string ActorName { get; set; }
        [JsonProperty("film-name")]
        public string FilmName { get; set; }
        [JsonProperty("actor-image")]
        public string ActorImage { get; set; }
    }

    public class CharacterViewModel
    {
        [JsonProperty("film-id")]
        public string FilmId { get; set; }
        [JsonProperty("film-name")]
        public string FilmName { get; set; }
        [JsonProperty("character-name")]
        public string CharacterName { get; set; }
        [JsonProperty("film-script")]
        public string FilmScript { get; set; }
    }
}
