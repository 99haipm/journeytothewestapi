﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.ViewModels
{
    class StatusConstants
    {
    }

   public class FilmStatus
   {
        public const string NEW = "New";
        public const string PROCESS = "Process";
        public const string FINISHED = "Finished";
        public const string DELETED = "Delete";
   }

    public class ToolStatus
    {
        public const string OUT_OF_STOCK = "Out of stock";
        public const string AVAILABLE = "Available";
        public const string DELETED = "Deleted";
    }
}
