﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.ViewModels
{
    public class HistoryGeneral
    {
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("modify-by")]
        public string ModifyBy { get; set; }
        [JsonProperty("date-modify")]
        public string DateModify { get; set; }

    }
}
