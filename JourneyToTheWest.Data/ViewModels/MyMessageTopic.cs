﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.ViewModels
{
    public class MyMessageTopic
    {
        [JsonProperty("topic")]
        public string Topic { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("body")]
        public string Body { get; set; }
    }
}
