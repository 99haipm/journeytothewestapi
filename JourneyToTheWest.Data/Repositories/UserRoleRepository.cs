﻿using JourneyToTheWest.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public interface IUserRoleRepository : IBaseRepository<UserRole, string>
    {
        UserRole AddRole(string userId, string roleId);
    }


    public class UserRoleRepository : BaseRepository<UserRole, string>, IUserRoleRepository
    {
        public UserRoleRepository(DbContext context) : base(context)
        {

        }

        public UserRole AddRole(string userId, string roleId)
        {
            var userRole = new UserRole
            {
                Id = Guid.NewGuid().ToString(),
                UserId = userId,
                RoleId = roleId
            };
            var result =  Create(userRole).Entity;
            SaveChanges();
            return result;
        }
    }
}
