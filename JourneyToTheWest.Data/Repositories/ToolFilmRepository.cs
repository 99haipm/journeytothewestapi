﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IToolFilmRepository : IBaseRepository<ToolFilm, string>
    {
        ToolFilm PrepareCreate(BorrowToolModel model, string filmId);
        ToolFilm CreateToolFilm(BorrowToolModel model, string filmId);
    }

    public partial class ToolFilmRepository :  BaseRepository<ToolFilm,string>, IToolFilmRepository
    {
        public ToolFilmRepository(DbContext context) : base(context)
        {

        }

        public ToolFilm CreateToolFilm(BorrowToolModel model, string filmId)
        {
            var currentToolFim = PrepareCreate(model, filmId);
            var result = Create(currentToolFim);
            return result.Entity;
        }

        public ToolFilm PrepareCreate(BorrowToolModel model, string filmId)
        {
            var currenToolFilm = new ToolFilm
            {
                Id = Guid.NewGuid().ToString(),
                BorrowFrom = model.BorrowFrom.ToFileTime(),
                BorrowTo = model.BorrowTo.ToFileTime(),
                Description = "",
                FilmId = filmId,
                Quantity = model.Quantity,
                ToolId = model.ToolId
            };
            return currenToolFilm;
        }
    }
}
