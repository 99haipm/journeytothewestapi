﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface ICharactorRepository : IBaseRepository<Character, string>
    {
        Character PrepareCreate(CharactorViewModel model,string filmScript);
        Character CreateCharacter(CharactorViewModel model, string filmScript);
    }

    public class CharactorRepository : BaseRepository<Character,string>, ICharactorRepository
    {
        public CharactorRepository(DbContext context): base(context)
        {

        }

        public Character CreateCharacter(CharactorViewModel model,string filmScript)
        {
            var result = PrepareCreate(model,filmScript);
            return Create(result).Entity;
        }

        public Character PrepareCreate(CharactorViewModel model,string filmScript)
        {
            var currentCharactor = new Character
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
                ActorId = model.ActorId,
                Description = "",
                FilmId = model.FilmId,
                FilmScript = filmScript
            };
            return currentCharactor;
        }
    }
}
