﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IToolRepository : IBaseRepository<Tool, string>
    {
        #region CREATE
        Tool PrepareCreate(ToolCreateModel model);
        Tool CreateTool(ToolCreateModel model, string image);
        #endregion


        #region UPDATE

        Tool UpdateTool(Tool currentTool, ToolUpdateModel model, string image);

        #endregion
    }

    public partial class ToolRepository : BaseRepository<Tool,string>, IToolRepository
    {
        public ToolRepository(DbContext context) : base(context)
        {

        }

        public Tool CreateTool(ToolCreateModel model,string image)
        {
            var newTool = PrepareCreate(model);
            newTool.Image = image;
            Create(newTool);
            return newTool;
        }

        public Tool PrepareCreate(ToolCreateModel model)
        {
            var newTool = new Tool
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
                Description = model.Description,
                Quantity = model.Quantity,
                Status = ToolStatus.AVAILABLE
            };
            return newTool;
        }

        public Tool UpdateTool(Tool currentTool, ToolUpdateModel model, string image)
        {
            currentTool.Name = model.Name != null ? model.Name :currentTool.Name;
            currentTool.Image = image != null ? image : currentTool.Image;
            currentTool.Description = model.Description != null ? model.Description : currentTool.Description;
            currentTool.Quantity = model.Quantity != 0 ? model.Quantity : currentTool.Quantity;
            return Update(currentTool).Entity;
        }
    }
}
