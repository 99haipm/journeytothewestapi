﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IUserRepository : IBaseRepository<Users, string>
    {
        Users CreateUser(UserCreateModel model);

        Users FindById(string id);
    }

    public partial class UserRepository : BaseRepository<Users, string>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {

        }

        public Users CreateUser(UserCreateModel model)
        {
            var user = new Users
            {
                Id = Guid.NewGuid().ToString(),
                Username = model.Username,
                Password = model.Password,
                Status = true,
                UpdatedProfile = false
            };
            return Create(user).Entity;
        }


        public Users FindById(string id)
        {
            return Get().FirstOrDefault(s => s.Id == id);
        }
    }
}
