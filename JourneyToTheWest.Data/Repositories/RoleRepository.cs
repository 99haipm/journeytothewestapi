﻿using JourneyToTheWest.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public interface IRoleRepository : IBaseRepository<Roles, string>
    {

    }


    public class RoleRepository : BaseRepository<Roles, string>, IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {

        }


    }
}
