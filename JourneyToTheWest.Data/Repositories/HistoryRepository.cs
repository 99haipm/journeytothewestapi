﻿using JourneyToTheWest.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface  IHistoryRepository : IBaseRepository<History,string>
    {
        History CreateHisotry(string userId, string desc);
    }

    public partial class HistoryRepository : BaseRepository<History,string>, IHistoryRepository
    {
        public HistoryRepository(DbContext context) : base(context)
        {

        }

        public History CreateHisotry(string userId, string desc)
        {
            var currentHistory = new History
            {
                Id = Guid.NewGuid().ToString(),
                Description = desc,
                ModifyBy = userId,
                ModifyDate = DateTime.UtcNow.AddHours(7),
                UserId = userId
            };
           return Create(currentHistory).Entity;
        }
    }


}
