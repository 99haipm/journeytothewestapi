﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public interface IActorRepository : IBaseRepository<Actor, string>
    {
        #region Create
        Actor PrepareCreate(ActorCreateModel model,string filename);
        Actor CreateActor(ActorCreateModel model,string filename);
        #endregion

        #region Edit
        Actor EditActor(ActorUpdateModel model, string id);
        #endregion


        #region Get
        Actor GetById(string id);
        #endregion
    }

    public class ActorRepository : BaseRepository<Actor, string>, IActorRepository
    {
        public ActorRepository(DbContext context) : base(context)
        {

        }

        public Actor CreateActor(ActorCreateModel model, string filename)
        {
            var actor = PrepareCreate(model,filename);
            return Create(actor).Entity;
        }

        public Actor EditActor(ActorUpdateModel model, string id)
        {
            var actor = GetById(id);
            actor.Name = model.Name !=null ? model.Name : actor.Name;
            actor.Description = model.Description != null ? model.Description : actor.Description;
            actor.Avatar = model.Avatar != null ? model.Avatar : actor.Avatar;
            actor.Email = model.Email != null ? model.Email : actor.Email;
            actor.Phone = model.Phone != null? model.Phone : actor.Phone;

            return Update(actor).Entity;
        }

        public Actor GetById(string id)
        {
            return Get().FirstOrDefault(s => s.Id == id);
        }

        public Actor PrepareCreate(ActorCreateModel model, string filename)
        {
            var actor = new Actor
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Avatar = filename,
                Email = model.Email,
                Phone = model.Phone,
                Status = true
            };
            return actor;
        }
    }

}
