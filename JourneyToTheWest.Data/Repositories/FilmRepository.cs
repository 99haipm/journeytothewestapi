﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IFilmRepository : IBaseRepository<Film,string>
    {
        #region CREATE

        Film PrepareCreate(FilmCreateModel model);
        Film CreateFilm(FilmCreateModel model);
        #endregion

        #region EDIT


        Film UpdateFilm(Film currentFilm, FilmUpdateModel model);

        #endregion

    }

    public class FilmRepository : BaseRepository<Film,string>, IFilmRepository
    {
        public FilmRepository(DbContext context) : base(context)
        {

        }

        public Film CreateFilm(FilmCreateModel model)
        {
            var newFilm = PrepareCreate(model);
            return newFilm;
        }

        public Film PrepareCreate(FilmCreateModel model)
        {

            var newFilm = new Film
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
                Description = model.Description,
                BeginTime = model.BeginTime.ToFileTime(),
                EndTime = model.EndTime.ToFileTime(),
                Location = model.Location,
                TimeOfFilm = 0,
                Status = FilmStatus.NEW
            };
            Create(newFilm);
            return newFilm;
        }

        public Film UpdateFilm(Film currentFilm, FilmUpdateModel model)
        {
            currentFilm.Name = model.Name;
            currentFilm.Location = model.Location;
            currentFilm.EndTime = model.EndTime != null ? model.EndTime.ToFileTime() : currentFilm.EndTime;
            currentFilm.BeginTime = model.BeginTime != null ? model.BeginTime.ToFileTime() : currentFilm.BeginTime;
            currentFilm.Description = model.Description;
            return currentFilm;
        }
    }
}
