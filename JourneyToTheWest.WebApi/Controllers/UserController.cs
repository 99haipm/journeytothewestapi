﻿using JourneyToTheWest.Data.Extensions;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebApi.Controllers
{
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private UserDomain _userDomain;
        private IHistoryRepository _historyRepo;
        public UserController(UserDomain userDomain, IHistoryRepository historyRepository)
        {
            _userDomain = userDomain;
            _historyRepo = historyRepository;
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] UserLoginModel model)
        {
            try
            {
                var currentUser = _userDomain.Login(model);
                if(currentUser != null)
                {
                    var token = _userDomain.GenerateToken(currentUser);
                    return Ok(token);
                }
                return BadRequest("Invalid Username or Password");
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Authorize]
        [HttpGet]
        public IActionResult GetUserInfo()
        {
            try
            {
                var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
                var result = _userDomain.GetCurrentUserDetail(token);
                if(result != null)
                {
                    return Ok(result);
                }
                return BadRequest("Error");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("register-account")]
        public IActionResult Register([FromBody] UserCreateModel model)
        {
            try
            {
                var check = _userDomain.CreateUser(model);
                if (check)
                {
                    return Ok();
                }
                return BadRequest("Create Fail");

            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-histories")]
        public IActionResult GetHistories (string userId)
        {
            try
            {
                var result = _historyRepo.Get().Where(s => s.UserId == userId)
                    .Select(s => new HistoryGeneral
                    {
                        DateModify = s.ModifyDate.ToString(),
                       Description = s.Description,
                       ModifyBy = s.ModifyByNavigation.Username
                    });


                if(result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
