﻿using Firebase.Auth;
using Firebase.Storage;
using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebApi.Controllers
{
    [Route("api/actor")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private ActorDomain _actorDomain;
       
        public ActorController(ActorDomain actorDomain)
        {
            _actorDomain = actorDomain;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] ActorFilter filter,
            [FromQuery]string sort,
            [FromQuery]int page = 0,
            [FromQuery]int limit = -1)
        {
            try
            {
                var result = _actorDomain.GetActor(filter, sort, page, limit);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult Create([FromBody] ActorCreateModel model)
        {
            try
            {
                    string id = _actorDomain.Create(model);
                    if (id != null)
                    {
                        return Ok(id);
                    }
                    return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut]
        public IActionResult Update([FromBody] ActorUpdateModel model, string id)
        {
                string currenId = _actorDomain.EditActor(id, model);
                if (currenId != null)
                {
                    return Ok(currenId);
                }
                return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public IActionResult Delete([FromQuery]string id)
        {
            try
            {
                _actorDomain.DeleteActor(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [Authorize(Roles = "Admin")]
        [HttpGet("active")]
        public IActionResult Active([FromQuery]string id)
        {
            try
            {
                _actorDomain.UnBanActor(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-by-id")]
        public IActionResult GetById(string id)
        {
            try
            {
                var result = _actorDomain.GetById(id);
                if(result != null)
                {
                    return Ok(result);
                }
                return BadRequest("null");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("set-up-account")]
        public async Task<IActionResult> SetUpAccount(IFormFile avatar)
        {
            try
            {
                //string ApiKey = "AIzaSyAWhzikYsmNrL83SZcSmcNnfJlUmiFMWaY";
                //string Bucket = "journey-to-the-west-edee1.appspot.com/";
                //string AuthEmail = "99haipm@gmail.com";
                //string AuthPassword = "12345678hai";

                ////await avatar.CopyToAsync(stream);
                //var auth = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));
                //var currentUser = await auth.SignInWithEmailAndPasswordAsync(AuthEmail, AuthPassword);

                //var cancellation = new CancellationTokenSource();

                //var task = new FirebaseStorage(
                //   Bucket,
                //   new FirebaseStorageOptions
                //   {
                //       AuthTokenAsyncFactory = () => Task.FromResult(currentUser.FirebaseToken),
                //       ThrowOnCancel = true // when you cancel the upload, exception is thrown. By default no exception is thrown
                //    })
                //   .Child("receipts")
                //   .Child("test")
                //   .Child($"aspcore.png")
                //   .PutAsync(null, cancellation.Token);



                if(avatar != null)
                {
                    var uploads = Path.Combine("Images", "Avatar");
                    var filePath = Path.Combine(uploads, avatar.FileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await avatar.CopyToAsync(fileStream);
                    }
                }
                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("avatar")]
        public IActionResult GetAvatar(string name)
        {
            Byte[] b;
            if (name == null)
            {
                return BadRequest("Not found");
            }
            b = System.IO.File.ReadAllBytes($"Images\\Avatar\\{name}");
            return File(b, "image/png");
        }
        
    }
}
