﻿using FirebaseAdmin.Messaging;
using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebApi.Controllers
{
    [ApiController]
    [Route("api/films")]
    public class FilmController : ControllerBase
    {
        private FilmDomain _filmDomain;
        private ToolFilmDomain _toolFilmDomain;
        public FilmController(FilmDomain filmDomain, ToolFilmDomain toolFilmDomain)
        {
            _filmDomain = filmDomain;
            _toolFilmDomain = toolFilmDomain;
        }



        [HttpPost("topic")]
        public async Task<IActionResult> SendNotificationToTopic([FromBody] MyMessageTopic inputMessage)
        {
            try
            {
                Message message = new Message()
                {
                    Topic = inputMessage.Topic,
                    Data = new Dictionary<string, string>
                    {
                        {"title" ,inputMessage.Title },
                        { "body", inputMessage.Body}
                    }
                };

                string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        [HttpDelete("delete-character")]
        public IActionResult DeleteCharacter(string characterId)
        {
            try
            {
                var check = _filmDomain.DeleteCharacter(characterId);
                if (check)
                {
                    return Ok();
                }
                return BadRequest("null");
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult CreateFilm([FromBody] FilmCreateModel model)
        {
            try
            {
                var newFilm = _filmDomain.CreateFilm(model);
                if(newFilm != null)
                {
                    return Ok(newFilm);
                }
                else
                {
                    return BadRequest("null");
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetFilm()
        {
            try
            {
                var result = _filmDomain.GetAll();
                if (result.Count > 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet("get-by-id")]
        public IActionResult GetById(string id)
        {
            try
            {
                var result = _filmDomain.GetAll().FirstOrDefault(s => s.Id ==id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("finish-film")]
        public IActionResult GetFilm([FromQuery] string filmID)
        {
            try
            {
                var result = _filmDomain.FinishFilm(filmID);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut]
        public IActionResult UpdateFilm([FromQuery] string filmID, [FromBody] FilmUpdateModel model)
        {
            try
            {
                var result = _filmDomain.UpdateFilm(model,filmID);
                if (result)
                {
                    return Ok(filmID);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult DeleteFilm([FromQuery] string filmID)
        {
            try
            {
                var result = _filmDomain.DeleteFilm(filmID);
                if (result)
                {
                    return Ok(filmID);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("add-tool-film")]
        public IActionResult AddToolFilm([FromBody]ToolFilmViewModel model)
        {
            try
            {
                var result = _toolFilmDomain.CreateToolFilms(model, model.FilmId);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-tool-of-film")]
        public IActionResult GetToolOfFilm(string filmId)
        {
            try
            {
                var result = _toolFilmDomain.GetToolsOfFilm(filmId);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("add-actor-to-film")]
        public async Task<IActionResult> AddActorToFilm([FromBody] CharactorViewModel model)
        {
            try
            {
                    var result = _filmDomain.AddActorToFilm(model);
                    if (result)
                    {
                    MyMessageTopic topic = new MyMessageTopic
                    {
                        Topic = "MyTopic_"+model.ActorId,
                        Body = "Bạn vừa Được thêm vào 1 phân cảnh",
                        Title = "Thông Báo"
                    };
                    await SendNotificationToTopic(topic);
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Null");
                    }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-actor-of-film")]
        public IActionResult GetActorOfFilm(string filmId)
        {
            try
            {

                var result = _filmDomain.GetCharactorOfFilm(filmId);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Null");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-script-character")]
        public IActionResult GetScriptCharacter(string scriptId)
        {
            try
            {
                var script = _filmDomain.GetFilmScrip(scriptId);
                if(script != null)
                {
                    return Ok(script);
                }
                else
                {
                    return BadRequest("Not found");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-character-of-actor")]
        public IActionResult GetCharacterOfActor(string actorId)
        {
            try
            {
                var result = _filmDomain.GetCharactorOfActor(actorId);
                if(result != null){
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-finish-film-actor")]
        public IActionResult GetFilmFinishOfActor(string actorId)
        {
            try
            {
                var result = _filmDomain.GetFinishFilmOfActor(actorId);
                if (result.Count > 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("get-film-of-actor")]
        public IActionResult GetFilmOfActor(string actorId)
        {
            try
            {
                var result = _filmDomain.GetAllFilmOfActor(actorId);
                if (result.Count >= 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("delete-tool-of-film")]
        public IActionResult DeleteToolOfFilm(string id)
        {
            try
            {
                var check = _toolFilmDomain.DeleteToolOfFilm(id);
                if (check)
                {
                    return Ok();
                }
                return BadRequest("null");
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
