﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebApi.Controllers
{
    [Route("api/tools")]
    [ApiController]
    public class ToolController : ControllerBase
    {
        private ToolDomain _toolDomain;
        public ToolController(ToolDomain toolDomain)
        {
            _toolDomain = toolDomain;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var result = _toolDomain.GetAll();
                if(result.Count >= 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult CreateTool([FromBody]ToolCreateModel model)
        {
            try
            {
                    var result = _toolDomain.CreateTool(model);
                    if (result != null)
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return BadRequest("null");
                    }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult UpdateTool([FromBody]ToolUpdateModel model,string id)
        {
            try
            {
                    var result = _toolDomain.EditTool(id, model);
                    if (result)
                    {
                        return Ok(id);
                    }
                    else
                    {
                        return BadRequest("null");
                    }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string id)
        {
            try
            {
                var result = _toolDomain.Delete(id);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("null");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("image")]
        public IActionResult GetImage(string name)
        {
            Byte[] b;
            if (name == null)
            {
                return BadRequest("Not found");
            }
            b = System.IO.File.ReadAllBytes($"Images\\Tool\\{name}");
            return File(b, "image/png");
        }


        [HttpGet("get-by-id")]
        public IActionResult GetById(string id)
        {
            try
            {
                var result = _toolDomain.GetAll().FirstOrDefault(s => s.id == id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("borrow-tool")]
        public IActionResult BorrowTool(string toolId, int quantity)
        {
            try
            {
                var result = _toolDomain.BorrowTool(toolId, quantity);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
